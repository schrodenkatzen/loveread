Simple lib to download books from loveread.ec for python

pip3 install loveread

python3

loveread.download("http://loveread.ec/read_book.php?id=70351") #returns book text
loveread.download("http://loveread.ec/read_book.php?id=70351", save=True) #downloads it to the script folder
loveread.download("http://loveread.ec/read_book.php?id=70351", save=True, folder="books/") #downloads it to the "books/" folder
