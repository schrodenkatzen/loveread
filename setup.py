import setuptools

with open("README.md", "r") as fh:

    long_description = fh.read()

    setuptools.setup(

       name='loveread',  

       version='0.1.1.1',

       packages=['loveread'],

       author="Vlad Havrilov",

       author_email="wladgavrilov@gmail.com",

       description="Simple lib to download books from loveread.ec",

       long_description=long_description,

       long_description_content_type="text/markdown",

       url="",

     #packages=setuptools.find_packages(),
     install_requires=[
     'hy','requests','bs4',
     ],
     package_data={'': ['loveread.hy']},
     include_package_data=True,
     classifiers=[

     "Programming Language :: Python :: 3",

     "License :: OSI Approved :: MIT License",

     "Operating System :: OS Independent",

     ],

     )
